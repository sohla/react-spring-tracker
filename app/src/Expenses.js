import React, {Component} from 'react'
import AppNav from './AppNav'
import DataPicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import './App.css'
import { Container, Form, FormGroup, Button } from 'reactstrap'
import {Link} from 'react-router-dom'
import Moment from 'react-moment'

export default class Expenses extends Component {

    emptyItem = {
        id: '103',
        expenseDate: new Date(),
        desciption: '',
        location: '',
        categories: [1, 'Travel']
    }

    constructor(props){
        super(props)
        this.state = {
            date: new Date(),
            isLoading: true,
            Expenses: [],
            Categories: [],
            item: this.emptyItem
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);

    }

    async handleSubmit(event){
        
        const item =this.state.item;
        await fetch('/api/expenses', {
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            },
            body: JSON.stringify(item),
        });
        event.preventDefault();
        this.props.history.push('/expenses');
        
    }


    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item={...this.state.item};
        item[name] = value;
        this.setState({item});
    
    }
    handleChange(date){
        let item={...this.state.item};
        item.expenseDate= date;
        this.setState({item});

    }

    async remove(id){
        await fetch('/api/expenses/${id}', {
            method: 'DELETE',
            headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(()=> {
            let updatedExpenses = [...this.state.Expenses].filter(i=> i.id !== id);
            this.setState({Expenses: updatedExpenses})
        });
    }



    async componentDidMount() {
        const response = await fetch('api/categories');
        const body = await response.json();
        this.setState({Categories: body, isLoading:false})

        const responseExp = await fetch('api/expenses');
        const bodyExp = await responseExp.json();
        this.setState({Expenses: bodyExp, isLoading:false})
    }

    render() {
        const title = <h3>Add expense</h3>
        const {Categories} = this.state;
        const {Expenses, isLoading} = this.state;

        if (isLoading)
            return(<div>loading...</div>)
        
        let optionList =   
                Categories.map((category)=>
                <option value={category.id} key={category.name}>
                    {category.name}
                </option>
            )
        
        let rows =   
            Expenses.map((expense)=>
            <tr key={expense.id}>
                <td>{expense.desciption}</td>
                <td>{expense.location}</td>
                <td> <Moment date={expense.expenseDate} format='YYYY/MM/DD'/></td>
                <td>{expense.category.names}</td>
                <td><Button size="sm" color="danger" onClick={() => this.remove(expense.id)}>delete</Button></td>
            </tr>
        )
        return (
            <div>
                <AppNav/>
                <Container>
                {title}
                    <Form onSubmit={this.handleSubmit}>

                        <FormGroup>
                            <label for="description">title</label>
                            <input type="description" name="title" id="title" onChange={this.handleChange} autoComplete="name"></input>
                        </FormGroup>

                        <FormGroup>
                            <label for="category">Category</label>
                            <selected>
                                <optionList/>
                            </selected>
                        </FormGroup>

                        <FormGroup>
                            <label for="date">Expense Date</label>
                            <DatePicker selected={this.state.item.expenseDate} onChange={this.handleDateChange}/>
                        </FormGroup>

                        <div className="row">
                        <FormGroup className="col-md-4 mb-3">
                            <label for="location">Location</label>
                            <input type="text" name="location" id="location" onChange={this.handleChange}/>
                        </FormGroup>
                        </div>

                        <FormGroup>
                            <Button color="primary" type="submit">Save</Button>{' '}
                            <Button color="secondary" tag={Link} to="/categories">Save</Button>{' '}

                        </FormGroup>

                    </Form>
                </Container>
                {' '}
                <Container>
                    <Table className="mt-4">
                    <thead>
                        <tr>
                            <th width="20%">description</th>
                            <th width="10%">Location</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th width="10%">Action</th>
                           
                             <tbody>
                                {rows}
                            </tbody>

                        </tr>
                    </thead>
                    </Table>
                </Container>
            </div>
        )
    }
}
