package repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Expense;

public interface ExpenseRepository extends JpaRepository<Expense, Long>  {

}
